package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{

    public ArrayList<FridgeItem> Fridgeitems = new ArrayList<FridgeItem>(20);
    int max_size = 20;


    @Override
    public int nItemsInFridge() {
        return Fridgeitems.size();
    }

    @Override
    public int totalSize() {
        return max_size;
    }

    @Override
    public boolean placeIn(FridgeItem item) {   
        if(nItemsInFridge() < totalSize()){
            Fridgeitems.add(item);
            return true;  
        }
        else{
            return false;
        }
    }

    @Override
    public void takeOut(FridgeItem item){
        if(nItemsInFridge() > 0){
            Fridgeitems.remove(item); 
        }
        else{
            throw new java.util.NoSuchElementException();
        }
    }

    @Override
    public void emptyFridge() {
        Fridgeitems.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expired = new ArrayList<>();
        for (int i = 0; i < nItemsInFridge(); i++){
            FridgeItem item = Fridgeitems.get(i);
            if(item.hasExpired()){
                expired.add(item);
            }
        }
        for(FridgeItem expiredfood : expired){
            Fridgeitems.remove(expiredfood);
        }
        return expired;
    }

}
